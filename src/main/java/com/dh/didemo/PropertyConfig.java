package com.dh.didemo;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

@Configuration
//@PropertySource("classpath:database.properties")
//@PropertySource({"classpath:database.properties", "classpath:jms.properties"})
//@PropertySources({@PropertySource("classpath:database.properties"), @PropertySource("classpath:jms.properties")})

public class PropertyConfig
{
    @Bean
    public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer()
    {
        return new PropertySourcesPlaceholderConfigurer();
    }
}
