package com.dh.didemo;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class FakeDataSource
{

    @Value("${database.user}")
    private String user;
    @Value("${database.password}")
    private String password;
    @Value("${database.url}")
    private String url;

    /*@Autowired
    private Environment env;*/

    public String getUser()
    {
        return user;
    }

    public void setUser(String user)
    {
        this.user = user;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    /*@PostConstruct
    public String envSalt()
    {
        return this.user = env.getProperty("ENV_USER");
    }*/
}
