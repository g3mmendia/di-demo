package com.dh.didemo.services;

public interface GreetingService {

    String sayGreeting();
}
