package com.dh.didemo.services;

import org.springframework.stereotype.Service;

@Service
public class GetterGreetingServiceImpl implements GreetingService {

    public static final String GREETING = "Hello GetterGreetingServiceImpl";

    @Override
    public String sayGreeting() {
        return GREETING;
    }
}
