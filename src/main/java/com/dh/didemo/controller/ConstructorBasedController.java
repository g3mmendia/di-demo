package com.dh.didemo.controller;

import com.dh.didemo.Forecast;
import com.dh.didemo.services.GreetingService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;

@Controller
public class ConstructorBasedController {
    private GreetingService greetingService;
    private Forecast forecast;

    public ConstructorBasedController(@Qualifier("constructorGreetingServiceImpl") GreetingService greetingService, Forecast forecast)
    {
        this.greetingService = greetingService;
        this.forecast = forecast;
    }

    public String sayHello() {
        return greetingService.sayGreeting() + " >> " + forecast.weather();
    }
}
