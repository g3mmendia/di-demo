package com.dh.didemo.controller;

import org.springframework.stereotype.Controller;

@Controller
public class MyController {
    public String hello() {
        System.out.println("Hello Spring");
        return "Hello Spring";
        /*String greeting = "Hello Spring";
        System.out.println();
        return greeting;*/
    }
}
