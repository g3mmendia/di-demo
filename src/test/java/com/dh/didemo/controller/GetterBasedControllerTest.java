package com.dh.didemo.controller;

import com.dh.didemo.services.GreetingServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class GetterBasedControllerTest {
    private GetterBasedController getterBasedController;

    @Before
    public void before() throws Exception {
        getterBasedController = new GetterBasedController();
        getterBasedController.setGreetingService(new GreetingServiceImpl());

    }

    @Test
    public void sayHello() {
        String greeting = getterBasedController.sayHello();
        Assert.assertEquals(GreetingServiceImpl.GREETING, greeting);
    }
}