package com.dh.didemo.controller;

import com.dh.didemo.Forecast;
import com.dh.didemo.services.GreetingService;
import com.dh.didemo.services.GreetingServiceImpl;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ConstructorBasedControllerTest {
    private ConstructorBasedController constructorBasedController;
    private Forecast forecast;

    @Before
    public void before() throws Exception {
        System.out.println("@Before");
        GreetingService greetingService = new GreetingServiceImpl();
        forecast = new Forecast();
        constructorBasedController = new ConstructorBasedController(greetingService, forecast);
    }

    @After
    public void tearDown() throws Exception {
        System.out.println("@After");
    }

    @Test
    public void sayHello() {
        String greeting = constructorBasedController.sayHello();
        Assert.assertEquals(GreetingServiceImpl.GREETING, greeting);
    }
}